# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/renanhangai_/nodejs/nestjs-validator/compare/v0.0.1...v0.0.2) (2019-06-28)



### 0.0.1 (2019-06-28)
