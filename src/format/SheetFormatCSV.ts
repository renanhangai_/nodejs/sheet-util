import {
	SheetFormatWriteOptions,
	SheetFormatReadOptions,
	SheetFormatVerifyOptions,
	SheetFormatReader,
	SheetFormatWriter,
} from "./SheetFormat";

export class SheetFormatCSV implements SheetFormatReader, SheetFormatWriter {
	/**
	 *
	 */
	verify(options: SheetFormatVerifyOptions) {
		return false;
	}
	/**
	 *
	 */
	read(options: SheetFormatReadOptions) {}
	/**
	 *
	 */
	write(options: SheetFormatWriteOptions) {}
}
