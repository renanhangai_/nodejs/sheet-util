import { Writable } from "stream";

export type SheetFormatVerifyOptions = {
	data: Buffer;
};
export type SheetFormatReadOptions = {
	data: Buffer;
};
export type SheetFormatWriteOptions = {
	stream: Writable;
};

export interface SheetFormatReader {
	/**
	 *
	 */
	verify(options: SheetFormatVerifyOptions): boolean | Promise<boolean>;
	/**
	 *
	 */
	read(options: SheetFormatReadOptions): void;
}

export interface SheetFormatWriter {
	/**
	 *
	 */
	write(options: SheetFormatWriteOptions): void;
}
