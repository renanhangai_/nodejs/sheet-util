import { SheetUtilInput, SheetUtilInputFile, SheetUtilInputStream } from "src/SheetUtil";
import fs from "fs";
import getStream from "get-stream";

export function toBuffer(input: SheetUtilInput): Promise<Buffer> {
	if ("filename" in input) {
		return toBufferFromFile(input);
	} else if ("buffer" in input) {
		return Promise.resolve(input.buffer);
	} else if ("stream" in input) {
		return toBufferFromStream(input);
	} else {
		throw new Error(`Invalid input. Must be a stream, buffer, or filename`);
		return Promise.resolve(null);
	}
}

function toBufferFromFile(input: SheetUtilInputFile): Promise<Buffer> {
	return new Promise((resolve, reject) => {
		fs.readFile(input.filename, { encoding: null }, (err, data) => {
			err ? reject(err) : resolve(data);
		});
	});
	return null;
}

function toBufferFromStream(input: SheetUtilInputStream): Promise<Buffer> {
	return getStream.buffer(input.stream);
}
