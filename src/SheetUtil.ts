import { Readable } from "stream";
import { SheetReader } from "./SheetReader";

export type SheetUtilInputFile = { filename: string };
export type SheetUtilInputBuffer = { buffer: Buffer };
export type SheetUtilInputStream = { stream: Readable };
export type SheetUtilInput = SheetUtilInputFile | SheetUtilInputBuffer | SheetUtilInputStream;

export type SheetUtilCreateReaderOptions = {
	input: SheetUtilInput;
};

export class SheetUtil {
	/**
	 * Cria o leitor de planilhas
	 * @param options
	 */
	async createReader(options: SheetUtilCreateReaderOptions): Promise<SheetReader> {
		return SheetReader.create(options);
	}

	/**
	 * Cria o escritor de planilhas
	 */
	async createWriter(): Promise<SheetReader> {
		return null;
	}
}
