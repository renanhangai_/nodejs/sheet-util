import { SheetUtilCreateReaderOptions } from "./SheetUtil";
import { toBuffer } from "./util/Buffer";
import { SheetFormatReader } from "./format/SheetFormat";

export class SheetReader {
	static async create(options: SheetUtilCreateReaderOptions): Promise<SheetReader> {
		const buffer = await toBuffer(options.input);
		if (!buffer) throw new Error(`Invalid input`);
		return null;
	}

	static async readDetect(options: SheetUtilCreateReaderOptions): Promise<SheetFormatReader> {
		return null;
	}
}
